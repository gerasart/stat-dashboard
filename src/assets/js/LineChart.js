import {Line} from 'vue-chartjs'

export default ({
    extends: Line,
    data() {
        return {
            gradient: null,
            gradient2: null
        }
    },
    mounted() {
        this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450);
        // this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450);

        // this.gradient2.addColorStop(0, 'rgba(0, 231, 255, 0.9)');
        // this.gradient2.addColorStop(0.5, 'rgba(0, 231, 255, 0.25)');
        // this.gradient2.addColorStop(1, 'rgba(0, 231, 255, 0)');

        this.gradient.addColorStop(0, 'rgba(255, 0,0, 0.5)');
        this.gradient.addColorStop(0.5, 'rgba(255, 0, 0, 0.25)');
        this.gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');

        this.renderChart({
            labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50],
            datasets: [
                {
                    label: '',
                    type:'line',
                    borderColor: 'rgb(83, 240, 233)',
                    pointBackgroundColor: 'rgb(83, 240, 233)',
                    borderWidth: 1,
                    pointRadius: 0,
                    fill: false,
                    lineTension: 0,
                    pointBorderColor: 'rgb(83, 240, 233)',
                    // backgroundColor: this.gradient,
                    data: [40, 39, 10, 40, 39, 80, 40, 3, 12, 44, 33, 22, 12, 21, 33, 31, 55, 43, 21, 66, 76, 44, 32, 23, 12, 55, 88, 75, 32, 23, 44, 5, 6, 77,55,56,58],
                },
                // {
                //     label: 'Data Two',
                //     borderColor: '#05CBE1',
                //     pointBackgroundColor: 'white',
                //     pointBorderColor: 'white',
                //     borderWidth: 1,
                //     backgroundColor: this.gradient2,
                //     data: [60, 55, 32, 10, 2, 12, 53]
                // }
            ],
        }, {
            responsive: true,
            maintainAspectRatio: false,
            scaleShowLabels : true,
        })
    }
})